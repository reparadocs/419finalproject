#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static const int size = 4;
int *goal_state;

typedef struct PuzzleState {
   int *board;
   int cost;
   int steps;
   struct PuzzleState *parent;
   struct PuzzleState *next;
} PuzzleState;

long hash_array(int board[]) {   
   int result = 1, i;
   for (i = 0; i < (size*size); ++i) {
      result = 31 * result + board[i];
   }
   return result;
}

int heuristic(int board[]) {
   int dist = 0, i;

   for (i = 0; i < (size * size); ++i) {
      int cur_row = i / size;
      int cur_col = i % size;
      int goal_row, goal_col;

      if (board[i] != 0) {
         goal_row = (board[i] - 1) / size;
         goal_col = (board[i] - 1) % size;
      } else {
         goal_row = size - 1;
         goal_col = size - 1;
     }

     dist += abs(goal_row - cur_row) + abs(goal_col - cur_col);
   }

   return dist;
}

int isGoal(int board[]) {
   return boardsAreEqual(board, goal_state);
}

int boardsAreEqual(int board1[], int board2[]) {
   int i;
   for (i = 0; i < (size * size); ++i) {
      if (board1[i] != board2[i]) {
         return 0;
      }
   }
   return 1;
}

int containsState(int board[], PuzzleState *ps) {
   while (ps != NULL) {
      if (boardsAreEqual(board, ps->board)) {
         return 1;
      }
      ps = ps->parent;
   }
   return 0;
}

PuzzleState *putInOpenList(PuzzleState *new_node, PuzzleState *head_ref) {
   PuzzleState *cur;
   if (head_ref == NULL || head_ref->cost >= new_node->cost) {
      new_node->next = head_ref;
      return new_node;
   } else {
      cur = head_ref;
      while (cur->next != NULL && cur->next->cost < new_node->cost) {
         cur = cur->next;
      }
      new_node->next = cur->next;
      cur->next = new_node;
      return head_ref;
   }
}

PuzzleState *astar(PuzzleState *curState) {
   PuzzleState *closedList = NULL;
   PuzzleState *openList = NULL;

   while (!isGoal(curState->board)) {
      int zeroIdx = -1, zero_row, zero_col, i;
      printf("board = [");
      for (i = 0; i < (size * size); ++i) {
         printf("%d, ", curState->board[i]);
      }
      printf("] - cost: %d - steps: %d\n", curState->cost, curState->steps);
      for (i = 0; i < (size * size); ++i) {
         if (curState->board[i] == 0) {
            zeroIdx = i;
         }
      }

      zero_row = zeroIdx / size;
      zero_col = zeroIdx % size;

      if (zero_row > 0) {
         PuzzleState *next = calloc(1, sizeof(PuzzleState));
         next->board = calloc(size * size, sizeof(int));
         memcpy(next->board, curState->board, (size * size) * sizeof(int));
         next->board[zeroIdx] = curState->board[zeroIdx - size];
         next->board[zeroIdx - size] = 0;
         next->cost = curState->steps + 1 + heuristic(next->board);
         next->steps = curState->steps + 1;
         next->parent = curState;
         next->next = NULL;
         if (!containsState(next->board, closedList)) {
            openList = putInOpenList(next, openList);
         }
      }
      if (zero_row < (size - 1)) {
         PuzzleState *next = calloc(1, sizeof(PuzzleState));
         next->board = calloc(size * size, sizeof(int));
         memcpy(next->board, curState->board, (size * size) * sizeof(int));
         next->board[zeroIdx] = curState->board[zeroIdx + size];
         next->board[zeroIdx + size] = 0;
         next->cost = curState->steps + 1 + heuristic(next->board);
         next->steps = curState->steps + 1;
         next->parent = curState;
         next->next = NULL;
         if (!containsState(next->board, closedList)) {
            openList = putInOpenList(next, openList);
         }
      }
      if (zero_col > 0) {
         PuzzleState *next = calloc(1, sizeof(PuzzleState));
         next->board = calloc(size * size, sizeof(int));
         memcpy(next->board, curState->board, (size * size) * sizeof(int));
         next->board[zeroIdx] = curState->board[zeroIdx - 1];
         next->board[zeroIdx - 1] = 0;
         next->cost = curState->steps + 1 + heuristic(next->board);
         next->steps = curState->steps + 1;
         next->parent = curState;
         next->next = NULL;
         if (!containsState(next->board, closedList)) {
            openList = putInOpenList(next, openList);
         }
      }
      if (zero_col < (size - 1)) {
         PuzzleState *next = calloc(1, sizeof(PuzzleState));
         next->board = calloc(size * size, sizeof(int));
         memcpy(next->board, curState->board, (size * size) * sizeof(int));
         next->board[zeroIdx] = curState->board[zeroIdx + 1];
         next->board[zeroIdx + 1] = 0;
         next->cost = curState->steps + 1 + heuristic(next->board);
         next->steps = curState->steps + 1;
         next->parent = curState;
         next->next = NULL;
         if (!containsState(next->board, closedList)) {
            openList = putInOpenList(next, openList);
         }
      }

      curState->next = closedList;
      closedList = curState;
      curState = openList;
      openList = curState->next;
   }

   return curState;
}

int main(int argc, char **argv) {
   goal_state = calloc(size * size, sizeof(int));
   int i;
   for (i = 0; i < (size*size) - 1; i++) {
      goal_state[i] = i+1;
   }
   goal_state[(size*size) - 1] = 0;
   PuzzleState *start = calloc(1, sizeof(PuzzleState));
   start->board = calloc(size * size, sizeof(int));
/*
   start->board[0] = 1;
   start->board[1] = 3;
   start->board[2] = 6;
   start->board[3] = 4;
   start->board[4] = 5;
   start->board[5] = 2;
   start->board[6] = 7;
   start->board[7] = 8;
   start->board[8] = 0;
*/
   start->board[0] = 6;
   start->board[1] = 7;
   start->board[2] = 11;
   start->board[3] = 13;
   start->board[4] = 3;
   start->board[5] = 5;
   start->board[6] = 15;
   start->board[7] = 14;
   start->board[8] = 8;
   start->board[9] = 9;
   start->board[10] = 0;
   start->board[11] = 10;
   start->board[12] = 12;
   start->board[13] = 4;
   start->board[14] = 1;
   start->board[15] = 2;
   start->cost = heuristic(start->board);
   start->steps = 0;
   start->parent = NULL;
   start->next = NULL;

   PuzzleState *solution = astar(start);
   printf("Total steps: %d\n", solution->steps);

   int j = -1;
   while (solution != NULL) {
      printf("board = [");
      for (i = 0; i < (size * size); ++i) {
         if (solution->board[i] == 0) {
            j = i;
         }
         printf("%d, ", solution->board[i]);
      }
      solution = solution->parent;
      if (solution != NULL)
         printf("] -- move: %d\n", solution->board[j]);
      else
         printf("]\n");
   }
}